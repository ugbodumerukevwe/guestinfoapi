# Guest Info System API Specification Overview

The GuestInfoAPI provides an OpenAPI specification that is implemented
by the GuestInfoBackend and called by the GuestInfoFrontend. The developers
of the Backend and the Frontend are the clients of GuestInfoAPI. GuestInfoAPI 
is deployed for use and under continuous development.

## Status
The system API is in active development and is currently deployed.

## Installation Instructions
TBD

## Usage Instructions

* [View or download the latest GuestInfoSystemAPI specification](docs/openapi.yaml)

Each specification file is written in [OpenAPI](https://www.openapis.org/),
a well-adopted, standardized language based on [YAML](https://yaml.org/).
There are many 3rd-party tools that work with OpenAPI specifications that
may be useful to frontend or backend development. Here are some references
to get you started.

* [OpenAPI Tools](https://openapi.tools/)
* [The Swagger Viewer extension for VS Code](
  https://marketplace.visualstudio.com/items?itemName=Arjun.swagger-viewer)
  renders the specification locally.
* GitLab provides a nice view of OpenAPI specification so long as it is
  stored in a single file named `openapi.yaml`.

## Tools

**Developer Guide**

1. Read [LibreFoodPantry.org](https://librefoodpantry.org/)
    and join its Discord server.
2. [Install development environment](docs/developer/install-development-environment.md)
3. Clone this repository using the following command

    ```bash
    git clone <repository-clone-url>
    ```

4. Open it in VS Code and reopen it in a devcontainer.
5. [specification/](specification/) contains the specification split over files.
  Its entry point is [specification/index.yaml](specification/index.yaml).
6. Familiarize yourself with the systems used by this project
  (see Development Infrastructure below).
7. See [the developer cheat-sheet](docs/developer/cheat-sheet.md) for common
  commands you'll likely use.

**Development Infrastructure**

* [Automated Testing](docs/developer/automated-testing.md)
* [Build System](docs/developer/build-system.md)
* [Continuous Integration](docs/developer/continuous-integration.md)
* [Dependency Management](docs/developer/dependency-management.md)
* [Development Environment](docs/developer/development-environment.md)
* [Documentation System](docs/developer/documentation-system.md)
* [Version Control System](docs/developer/version-control-system.md)
* Visual Studio Code
* Docker Desktop

# License

[GPL v3](LICENSE)
